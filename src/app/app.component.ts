import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from './_services/user.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  user: string;
  userSubscription: Subscription;
  constructor(
    private userService: UserService
  ) {
    this.userSubscription = this.userService.getUserObservable().subscribe( user => {
      this.user = user;
    });
  }
  ngOnInit() {
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }
}
