import {Inject, Injectable, Injector, PLATFORM_ID} from '@angular/core';
import {UserService} from '../_services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AppLoadService {

  constructor(
    private injector: Injector,
    @Inject(PLATFORM_ID) private platformId,
    private userService: UserService
  ) {}

  async initApp(): Promise<boolean> {
    // Function that will run on app initialization
    console.log('initApp');
    this.userService.setUser('Evgeny');
    return true;
  }}
