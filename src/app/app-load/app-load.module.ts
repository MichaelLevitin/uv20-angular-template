import {APP_INITIALIZER, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppLoadService} from './app-load.service';

// Hook into App initialization process and post token and request app settings before anything loads
export function init_app(appLoadService: AppLoadService) {
  return () => appLoadService.initApp(); // must return Promise
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
  ]
})
export class AppLoadModule { }
