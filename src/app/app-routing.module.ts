import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestGuard} from './_guards/test.guard';


const routes: Routes = [
  {
    path: '', children: [
      {
        path: '',
        pathMatch: 'full',
        loadChildren: () => import('./test/test.module').then( mod => mod.TestModule),
        canActivate: [TestGuard]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
