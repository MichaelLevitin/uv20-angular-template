import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: string;
  userSubject: BehaviorSubject<string>;
  constructor() {
    this.userSubject = new BehaviorSubject('');
  }
  setUser(value) {
    this.user = value;
    this.emitUser();
  }
  getUserObservable() {
    return this.userSubject.asObservable();
  }
  emitUser() {
    this.userSubject.next(this.user);
  }
}
